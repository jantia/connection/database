<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Adapter\Connection\Kafka;

//
use Tiat\Connection\Database\Adapter\Connection\AbstractDatabaseConnection;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Connection extends AbstractDatabaseConnection {

}
