<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Adapter\Driver\ElasticSearch;

//
use Tiat\Connection\Database\Adapter\Driver\AbstractDatabaseDriver;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Driver extends AbstractDatabaseDriver {
	
}
