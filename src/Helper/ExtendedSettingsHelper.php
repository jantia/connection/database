<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Helper;

//
use Tiat\Connection\Database\Helper\SettingsHelper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait ExtendedSettingsHelper {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use SettingsHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DRIVER_ELASTICSEARCH = 'elasticsearch';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DRIVER_KAFKA = 'kafka';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DRIVER_RABBIT_MQ = 'rabbit_mq';
}
