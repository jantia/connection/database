<?php

/**
 * Jantia
 *
 * @package        Jantia\Connection\Database
 * @license        Proprietary
 */

declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Helper;

//
use function ctype_digit;
use function iconv;
use function is_scalar;
use function mb_detect_encoding;
use function mb_ereg_replace;
use function serialize;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait DatabaseValidatorTrait {
	
	/**
	 * @param    mixed|NULL    $param
	 * @param    bool          $convert
	 *
	 * @return null|int|string
	 * @since   3.0.0 First time introduced.
	 */
	final public function checkParam(mixed $param = NULL, bool $convert = TRUE) : int|string|null {
		// if $param is NULL then return it directly
		if($param === NULL):
			return $param;
		endif;
		
		// If param is not scalar then serialize it
		if(! is_scalar($param)):
			$param = serialize($param);
		endif;
		
		// Check the param
		$param = $this->bindParam($param, $convert);
		
		// Use integer only when first number is NOT null/zero (casting to integer will discard first character if it's the null/zero)
		if(ctype_digit($param) && $param[0] !== "0"):
			return (integer)$param;
		else:
			return "'" . $param . "'";
		endif;
	}
	
	/**
	 * @param    null|string    $param
	 * @param    bool           $convert
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	final public function bindParam(?string $param, bool $convert = TRUE) : ?string {
		if($param === NULL):
			return NULL;
		elseif($convert):
			return $this->_bindParam($param);
		else:
			// Convert string to used encoding
			if(mb_detect_encoding($param) !== $this->getEncoding()):
				$param = iconv($this->getEncoding(), ( mb_detect_encoding($param) ), $param);
			endif;
			
			//
			return $this->_bindParam($param);
		endif;
	}
	
	/**
	 * @param    mixed    $param
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	private function _bindParam(mixed $param) : string {
		return mb_ereg_replace('[\x00\x0A\x0D\x1A\x22\x25\x27\x5C\x5F]', '\\\0', $param);
	}
}
