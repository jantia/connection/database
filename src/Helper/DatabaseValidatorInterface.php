<?php

/**
 * Jantia
 *
 * @package        Jantia\Connection\Database
 * @license        Proprietary
 */

declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Helper;

//
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DatabaseValidatorInterface extends ParametersPluginInterface {
	
	/**
	 * @param    mixed|NULL    $param
	 * @param    bool          $convert
	 *
	 * @return null|int|string
	 * @since   3.0.0 First time introduced.
	 */
	public function checkParam(mixed $param = NULL, bool $convert = TRUE) : int|string|null;
	
	/**
	 * @param    null|string    $param
	 * @param    bool           $convert
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function bindParam(?string $param, bool $convert = TRUE) : ?string;
}
