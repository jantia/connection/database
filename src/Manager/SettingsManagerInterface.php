<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Manager;

//
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Standard\Config\ConfigInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface SettingsManagerInterface {
	
	/**
	 * @param    ConfigInterface|iterable    $config
	 * @param    iterable|NULL               $options
	 */
	public function __construct(ConfigInterface|iterable $config, iterable $options = NULL);
	
	/**
	 * @return ConfigInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfig() : ConfigInterface;
	
	/**
	 * @return null|DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettingsInterface() : ?DatabaseSettingsInterface;
	
	/**
	 * @param    DatabaseSettingsInterface    $interface
	 *
	 * @return SettingsManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettingsInterface(DatabaseSettingsInterface $interface) : SettingsManagerInterface;
	
	/**
	 * @return SettingsManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function parseConfig() : SettingsManagerInterface;
}
