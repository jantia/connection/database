<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Manager;

//
use Jantia\Connection\Database\Exception\InvalidArgumentException;
use Jantia\Connection\Database\Helper\ExtendedSettingsHelper;
use Tiat\Config\Config;
use Tiat\Connection\Database\Adapter\Settings\DatabaseSettingsInterface;
use Tiat\Connection\Database\Adapter\Settings\Mysqli\DatabaseSettingsMysqli;
use Tiat\Connection\Database\Adapter\Settings\Postgres\DatabaseSettingsPostgres;
use Tiat\Connection\Database\Helper\SettingsInterface;
use Tiat\Connection\Stdlib\Adapter\ConnectionParamKeywords;
use Tiat\Standard\Config\ConfigInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class SettingsManager implements ParametersPluginInterface, SettingsInterface, SettingsManagerInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ExtendedSettingsHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @var ConfigInterface
	 */
	private ConfigInterface $_config;
	
	/**
	 * @var DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	private DatabaseSettingsInterface $_settingsInterface;
	
	/**
	 * @param    ConfigInterface|iterable    $config
	 * @param    null|iterable               $options
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(ConfigInterface|iterable $config, iterable $options = NULL) {
		//
		if(! $config instanceof ConfigInterface):
			$config = new Config($config);
		endif;
		
		//
		$this->_config = $config;
		
		//
		$this->setConnectionOptions($options);
	}
	
	/**
	 * @return null|DatabaseSettingsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getSettingsInterface() : ?DatabaseSettingsInterface {
		//
		if(empty($this->_settingsInterface)):
			$this->parseConfig();
		endif;
		
		//
		return $this->_settingsInterface ?? NULL;
	}
	
	/**
	 * @param    DatabaseSettingsInterface    $interface
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setSettingsInterface(DatabaseSettingsInterface $interface) : static {
		//
		$this->_settingsInterface = $interface;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function parseConfig() : static {
		//
		if(! empty($driver = $this->getConfig()?->getParam(ConnectionParamKeywords::DRIVER->value)) &&
		   ! empty($params = $this->getConfig()?->getParams())):
			return $this->setSettingsInterface(match ( strtolower($driver ?? '') ) {
				self::DRIVER_MARIADB, self::DRIVER_MYSQLI, self::DRIVER_MYSQL => new DatabaseSettingsMysqli($params,
				                                                                                            $this->getConnectionOptions()),
				self::DRIVER_POSTGRES => new DatabaseSettingsPostgres($params, $this->getConnectionOptions()),
				default => throw new InvalidArgumentException(sprintf("Given driver %s is not supported.", $driver))
			});
		endif;
		
		//
		throw new InvalidArgumentException(sprintf("You have to define driver (%s) variable.",
		                                           ConnectionParamKeywords::DRIVER->value));
	}
	
	/**
	 * @return ConfigInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfig() : ConfigInterface {
		return $this->_config;
	}
}
