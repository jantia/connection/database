<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Database
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Database\Manager;

//
use Jantia\Connection\Database\Adapter\Connection\ElasticSearch\Connection as ElasticSearchConnection;
use Jantia\Connection\Database\Adapter\Connection\Kafka\Connection as KafkaConnection;
use Jantia\Connection\Database\Adapter\Connection\RabbitMQ\Connection as RabbitMQConnection;
use Jantia\Connection\Database\Adapter\Driver\ElasticSearch\Driver as ElasticSearch;
use Jantia\Connection\Database\Adapter\Driver\Kafka\Driver as Kafka;
use Jantia\Connection\Database\Adapter\Driver\RabbitMQ\Driver as RabbitMQ;
use Jantia\Connection\Database\Exception\RuntimeException;
use Jantia\Connection\Database\Helper\DatabaseValidatorInterface;
use Jantia\Connection\Database\Helper\DatabaseValidatorTrait;
use Jantia\Connection\Database\Helper\ExtendedSettingsHelper;
use Tiat\Connection\Database\Adapter\DatabaseAdapter;
use Tiat\Connection\Database\Adapter\Driver\DatabaseDriverInterface;
use Tiat\Connection\Database\Query\DatabaseQueryInterface;

use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DatabaseManager extends DatabaseAdapter implements DatabaseValidatorInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use DatabaseValidatorTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ExtendedSettingsHelper;
	
	/**
	 * @param    int|string    $name
	 *
	 * @return null|DatabaseQueryInterface
	 */
	public function getQuery(int|string $name) : ?DatabaseQueryInterface {
		if(! empty($driver = $this->_getSettings()?->getDriver())):
			return match ( $driver ) {
				self::DRIVER_ELASTICSEARCH, self::DRIVER_KAFKA, self::DRIVER_RABBIT_MQ => throw new RuntimeException(sprintf("Driver %s implementation is still going on.",
				                                                                                                             $driver)),
				default => parent::getQuery($name)
			};
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    string    $driver
	 *
	 * @return DatabaseDriverInterface
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _createDriver(string $driver) : DatabaseDriverInterface {
		if(! empty($driver)):
			return match ( $driver ) {
				self::DRIVER_ELASTICSEARCH => new ElasticSearch(new ElasticSearchConnection($this->_getSettings(),
				                                                                            $this->getConnectionOptions())),
				self::DRIVER_KAFKA => new Kafka(new KafkaConnection($this->_getSettings(),
				                                                    $this->getConnectionOptions())),
				self::DRIVER_RABBIT_MQ => new RabbitMQ(new RabbitMQConnection($this->_getSettings(),
				                                                              $this->getConnectionOptions())),
				default => parent::_createDriver($driver)
			};
		endif;
		
		//
		return parent::_createDriver($driver);
	}
}
